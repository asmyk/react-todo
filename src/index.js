import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from 'components/App/App.jsx';
import {NotSupportStorage} from "./pages/NotSupportStorage/NotSupportStorage"
import registerServiceWorker from './registerServiceWorker';
import "normalize.css";
import {initStore} from "services/LocalStorageService";


function isStorageSupported(){
    let testItem = "test";
    try {
        localStorage.setItem(testItem, testItem);
        localStorage.removeItem(testItem);
        return true;
    } catch(e) {
        return false;
    }
}

// Storage is a key feature for this app. If Browser don't support storage - just show info. It can be extended to failback in real-world app
if(isStorageSupported()) {
    initStore();

    ReactDOM.render(<App/>, document.getElementById('root'));
    registerServiceWorker();

}else{
    ReactDOM.render(<NotSupportStorage/>, document.getElementById('root'));
}