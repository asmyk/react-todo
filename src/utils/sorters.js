const priorities = {"low": 0, "normal": 1, "high": 2}

const sorters = {
  dateOldToNew: function dateOldToNew(a, b) {
    return (new Date(a.date) < new Date(b.date));
  },
  dateNewToOld: function dateNewToOld(a, b) {
    return (new Date(a.date) > new Date(b.date));
  },
  priorityLowToHigh: function priorityLowToHigh(a, b) {
    return (priorities[a.priority] > priorities[b.priority]);
  },
  priorityHighToLow: function priorityHighToLow(a, b) {
    return (priorities[a.priority] < priorities[b.priority]);
  },
  completedFirst: function completedFirst(a, b) {
    return !a.isCompleted ;
  },
  notCompletedFirst: function notCompletedFirst(a, b) {
    return a.isCompleted;
  }
};

const sortOptions = [
  {label: "New to old", value: sorters.dateNewToOld.name},
  {label: "Old to new", value: sorters.dateOldToNew.name},
  {label: "Priority - low to high", value: sorters.priorityLowToHigh.name},
  {label: "Priority - high to low", value: sorters.priorityHighToLow.name},
  {label: "Completed first", value: sorters.completedFirst.name},
  {label: "Completed last", value: sorters.notCompletedFirst.name},
];


export {sorters, sortOptions};