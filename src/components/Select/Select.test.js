import React from 'react';
import ReactDOM from 'react-dom';
import {Select} from './Select';
import {createMemoryHistory} from 'history'

describe("select component", function () {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Select options={[{label: "1", value: "2"}]}/>, div);
  });

});
