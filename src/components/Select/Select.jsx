import React, {Component} from "react";
import PropTypes from 'prop-types';
import style from "./style.css"

/**
 * Render <select /> tag.
 * Available props:
 * selected - value of selected option
 * options - array with options. ex. [{value:"myval", label:"mylabel"}]
 * attrs - html attributes
 * handleChange - callback function for change event
 */
class Select extends Component {
  render() {
    let {selected, options, attrs, handleChange} = this.props;

    return <select className={style.select} {...attrs} value={selected} onChange={handleChange}>
      {options.map(option => (
        <option key={"id-" + option.value + option.label}
                value={option.value}>{option.label}</option>))}
    </select>
  }
}


Select.propTypes = {
  selected: PropTypes.string,
  attrs: PropTypes.object,
  options: PropTypes.array.isRequired,
  handleChange: PropTypes.func
}

export {Select};