import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {Button} from "components/Button/Button.jsx";
import style from "./style.css";
import routes from "utils/routes.js";

/**
 * Component for main app header. Shown on top of the app
 **/
class PageHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentWillMount() {
    let {isAuthenticated} = this.props.auth;

    if (isAuthenticated()) {
      this.setState({profile: {}});
      const {userProfile, getProfile} = this.props.auth;
      if (!userProfile) {
        getProfile((err, profile) => {
          this.setState({profile});
        });
      } else {
        this.setState({profile: userProfile});
      }
    }
  }

  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout();
  }


  render() {
    let {isAuthenticated} = this.props.auth,
      {profile} = this.state;

    return (<div className={style.header}>
        <Link to={routes.HOME} className={style.logo}> TodoApp </Link>

        {
          !isAuthenticated() && (
            <Button
              type="flat"
              handleOnClick={this.login}
            >
              Log In
            </Button>
          )
        }

        {isAuthenticated() && <span>Logged as: {profile.name} </span>}
        {
          isAuthenticated() && (
            <Button
              type="flat"
              handleOnClick={this.logout}
            >
              Log Out
            </Button>
          )
        }
      </div>
    );
  }
}

export {PageHeader};