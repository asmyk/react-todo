import React from 'react';
import style from "./style.css";

/**
 * Heading Title text component
 **/
function Title(props) {
  return (<h1 className={style.title}>
      {props.children}
    </h1>
  );
}

export {Title};