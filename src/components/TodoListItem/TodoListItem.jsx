import React, {Component} from "react";
import PropTypes from 'prop-types';
import style from "./style.css";

class TodoListItem extends Component {
  constructor(props) {
    super(props);

    this.state = {isCompleted: props.isCompleted, isExpanded: false}
    this.handleClick = this.handleClick.bind(this);
    this.handleCompletedClick = this.handleCompletedClick.bind(this);
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
  }

  /**
   * Set correct style for given task priority
   */
  getPriorityClass(priority) {
    switch (priority) {
      case "low":
        return style.lowPriority;
      case "normal":
        return style.normalPriority;
      case "high":
        return style.highPriority;
      default:
        return style.normalPriority;
    }
  }
  /**
   * User clicks remove task button
   */
  handleRemoveClick() {
    this.props.handleRemove(this.props.id);
  }

  /**
   * User sets given task to be completed
   */
  handleCompletedClick(e) {
    let {title, priority, note, date, id} = this.props,
      isCompleted = e.target.checked;

    this.props.handleUpdate({title, priority, note, date, id, isCompleted});
    this.setState({isCompleted})
  }

  /**
   * User clicks on task bar - it expand details if necessary
   */
  handleClick(e) {
    // check if user clicked inside on bar, not on input or link
    if (e.target.tagName === "DIV") {
      this.setState({isExpanded: !this.state.isExpanded})
    }
  }

  render() {
    let {title, priority, note, date, id, isAuthenticated} = this.props,
      // class depends on tile priority and isCompleted
      containerClass = [style.container, this.state.isCompleted ? style.completed : this.getPriorityClass(priority)].join(" "),
      noteTextClass = this.state.isExpanded ? style.noteTextExpanded : style.noteText,
      shareText = "hey! I want share my task: ".concat(title),
      // show twitter button for sharing task - only for authenticated users
      shareComponent = isAuthenticated ? <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" data-text={shareText}
                                            className="twitter-share-button"
                                            data-show-count="false">Tweet</a> : "Log-in to share";

    return <div onClick={this.handleClick} className={containerClass}>
      <input className={style.switch} type="checkbox" value="" checked={this.state.isCompleted} id={"isCompleted" + id}
             onChange={this.handleCompletedClick}/>
      <div>
        {title} <span className={style.dateText}>({date})</span>
      </div>

      <div className={noteTextClass}>
        {note}
      </div>

      {shareComponent}

      <div>

        <a title="Remove task" role="button" tabIndex="0" className={style.actionLink}
           onClick={this.handleRemoveClick}>X</a>
      </div>

    </div>
  }
}

TodoListItem.defaultProps = {
  handleUpdate: () => {
  },
  handleRemove: () => {
  }
};

TodoListItem.propTypes = {
  title: PropTypes.string.isRequired,
  priority: PropTypes.string,
  isAuthenticated: PropTypes.bool.isRequired,
  note: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  isCompleted: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  handleUpdate: PropTypes.func,
  handleRemove: PropTypes.func
};

export {TodoListItem}