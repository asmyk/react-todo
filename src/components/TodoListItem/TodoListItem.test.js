import React from 'react';
import ReactDOM from 'react-dom';
import {TodoListItem} from './TodoListItem';
import TestUtils from 'react-dom/test-utils';

let requiredProps = {
  title: "Test",
  isAuthenticated: false,
  note: "test",
  date: "2012-2-2",
  isCompleted: false,
  id: "123"
};

describe("TodoListItem component", function () {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<TodoListItem {...requiredProps}/>, div);
  });

  it("should handles update", function () {
    const handleUpdate = jest.genMockFunction();

    const component = TestUtils.renderIntoDocument(<TodoListItem handleUpdate={handleUpdate} {...requiredProps}/>);
    const updateCheckbox = TestUtils.scryRenderedDOMComponentsWithTag(
      component, "input"
    );
    TestUtils.Simulate.change(updateCheckbox[0]);

    expect(handleUpdate).toBeCalled();
  });

  it("should handle remove click", function () {
    const handleRemove = jest.genMockFunction();

    const component = TestUtils.renderIntoDocument(<TodoListItem handleRemove={handleRemove} {...requiredProps}/>);
    const removeTodo = TestUtils.scryRenderedDOMComponentsWithTag(
      component, "a"
    );
    TestUtils.Simulate.click(removeTodo[0]);

    expect(handleRemove).toBeCalled();
  })
});
