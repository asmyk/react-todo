import React from "react"

function EmptyList(props) {
  return <div>{props.text}</div>
}

export {EmptyList}