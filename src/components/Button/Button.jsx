import React from "react";
import style from "./style.css";

function getButtonStyleClass(type) {
  let className = "";

  switch (type) {
    case "success":
      className = style.success;
      break;
    case "danger":
      className = style.danger;
      break;
    case "flat":
      className = style.flat;
      break;
    default:
      className = style.default
      break;
  }

  return className;
}


/**
 * Button component which wraps <button> tag. You can specify style and handle click event
 * @param props:
 *  - attrs - additional HTML attributes.
 *  - type - button type. available types: success, flat, danger or default
 *  - handleOnClick - callback for click event
 *
 *  Example:
 *  <Button attrs={{title: 'This is my button}} type="success" handleOnClick={onClickCallback}>Click me</Button
 */
function Button(props) {
  let {children, type, attrs, handleOnClick} = props,
    className = [style.btn, getButtonStyleClass(type)].join(" ");

  return (<button {...attrs} onClick={handleOnClick} className={className}>{children}</button>)
}

export {Button}