import React from 'react';
import ReactDOM from 'react-dom';
import {Button} from './Button';
import TestUtils from 'react-dom/test-utils';


describe("Button component", function () {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Button/>, div);
  });

  it("should handles click", function () {
    const handleClick = jest.genMockFunction();

    const component = TestUtils.renderIntoDocument(<Button handleOnClick={handleClick}/>);
    const button = TestUtils.findRenderedDOMComponentWithTag(
      component, "button"
    );

    TestUtils.Simulate.click(button[0]);

    expect(handleClick).toBeCalled();
  });

});
