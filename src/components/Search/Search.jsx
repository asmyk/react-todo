import React, {Component} from "react";
import debounce from "throttle-debounce/debounce"
import PropTypes from 'prop-types';
import style from "./style.css"

/**
 * Render <input /> tag for search purposes.
 * Available props:
 * selected - value of selected option
 * handleChange - callback function for change event
 */
class Search extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.debouncedCallback = debounce(200, this.props.handleChange, this.props.handleChange);
  }

  handleChange(e) {
    this.debouncedCallback(e.target.value)
  }

  render() {
    let {value} = this.props;

    return <input className={style.searchInput} name="search" placeholder="Filter tasks by note" value={value}
                  onKeyUp={this.handleChange}/>
  }
}

Search.defaultProps = {
  handleChange: () => {
  }
};

Search.propTypes = {
  value: PropTypes.string,
  handleChange: PropTypes.func
};

export {Search};