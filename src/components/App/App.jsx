import React from 'react';
import {Router, Route, Redirect} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'


import {PageHeader} from "components/PageHeader/PageHeader.jsx";
import {Home} from "pages/Home/Home.jsx";
import {AddTodo} from "pages/AddTodo/AddTodo.jsx";
import Auth from 'containers/Auth/Auth';
import style from "./style.css"

import routes from "utils/routes.js";

const auth = new Auth();

const handleAuthentication = ({location}) => {
  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }
};

const customHistory = createBrowserHistory();

function App() {
  return <Router history={customHistory}>
    <div>
      <PageHeader auth={auth}/>

      <div className={style.appContent}>
        <Route exact path={routes.HOME} render={(props) => <Home auth={auth} {...props} />}/>
        <Route path={routes.ADD_TODO} render={(props) => <AddTodo auth={auth} {...props} />}/>
        <Route path="/callback" render={(props) => {
          handleAuthentication(props);
          return <Redirect to={routes.HOME} {...props} />
        }}/>
      </div>
    </div>
  </Router>
}

export default App;
