import React, {Component} from "react";
import {Select} from "components/Select/Select.jsx";
import {sortOptions} from "utils/sorters.js"

class Sort extends Component {
  constructor(props) {
    super(props);
    this.handleSortChange = this.handleSortChange.bind(this);
  }

  handleSortChange(event) {
    this.props.handleSortBy(event.target.value);
  }

  render() {
    let {sortStrategy} = this.props

    return (
      <Select attrs={{name: "sort-strategy"}} handleChange={this.handleSortChange}
              selected={sortStrategy}
              options={sortOptions}/>

    )
  }
}

export {Sort};