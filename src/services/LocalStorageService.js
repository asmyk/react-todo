import uuid from "node-uuid";

const appKey = "todoApp"

/**
 * Save app state to localStorage
 * @param state
 */
function saveState(state) {
  try {
    let serializedState = JSON.stringify(state)
    localStorage.setItem(appKey, serializedState)
  } catch (error) {
    return error;
  }
}

/**
 * Add todo to local storate state OR update existing todo if id is provided
 * @param newTodo
 */
function saveTodo(newTodo) {
  if (!newTodo) {
    throw new Error("You have to pass and object with filled data to add to-do");
  }

  let state = getState(),
    id = newTodo.id || uuid.v4();

  // update existing item if id is passed

  if (!state[id]) {
    newTodo.id = id; // store id inside newTodo
    newTodo.isCompleted = false;
  }

  state[id] = newTodo;

  saveState(state);
}

/**
 * Removes given todo entry from app state
 * @param id
 */
function removeTodo(id) {
  let state = getState();

  if (state[id]) {
    delete state[id];
    saveState(state);
  }
}

/**
 * Returns current app state
 */
function getState() {
  try {
    let serializedState = localStorage.getItem(appKey)
    if (!serializedState) {
      return undefined;
    }
    return JSON.parse(serializedState)
  } catch (error) {
    return error;
  }
}

/**
 * If state not exist in localStorage, then initialize empty state in localStorage
 */
function initStore() {
  let state = getState()
  if (!state || typeof state !== "object") {
    state = {};
    saveState(state);
  }
}


export {initStore, getState, saveTodo, removeTodo}
