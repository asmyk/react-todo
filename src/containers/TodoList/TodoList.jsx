import React, {Component} from "react";
import PropTypes from 'prop-types';
import debounce from "throttle-debounce/debounce"
import {TodoListItem} from "components/TodoListItem/TodoListItem.jsx";
import {EmptyList} from "components/EmptyList/EmptyList.jsx";
import {sorters} from "utils/sorters.js"


/**
 * Sortable todo list component
 */
class TodoList extends Component {
  constructor(props) {
    super(props)

    let bound = this.handleUpdateItem.bind(this);
    this.handleUpdateItem = debounce(500, bound, bound)
  }

  handleUpdateItem(e) {
    this.props.handleUpdateItem(e);
  }


  render() {
    let {sortStrategy, todoList = [], searchText} = this.props,
      listComponent;

    // sort with selected sort strategy
    todoList.sort(sorters[sortStrategy]);

    // if search text is given - filter sorted list by text
    if (searchText) {
      todoList = todoList.filter(function ({note}) {
        return note.toLowerCase().indexOf(searchText) !== -1;
      });
    }

    // when todolist is empty - show info about that
    if (todoList.length === 0) {
      listComponent = (this.props.todoList.length > 0) ? <EmptyList text="Not found any to-do for given filters"/> :
        <EmptyList text="Add your first to-do task using add button on the right :)"/>;
    } else {
      listComponent = todoList.map(item => (<TodoListItem
        handleRemove={this.props.handleRemoveItem}
        handleUpdate={this.handleUpdateItem}
        isAuthenticated={this.props.isAuthenticated}
        {...item}
        key={item.id}/>  )
      )
    }

    return <div>{listComponent}</div>
  }
}

TodoList.propTypes = {
  todoList: PropTypes.array.isRequired,
  sortStrategy: PropTypes.string.isRequired,
  handleUpdateItem: PropTypes.func,
  searchText: PropTypes.string,
  handleRemoveItem: PropTypes.func,
  isAuthenticated: PropTypes.bool.isRequired
}

export {TodoList};