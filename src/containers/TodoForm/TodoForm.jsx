import React, {Component} from "react";
import escape from "escape-html";
import {Button} from "components/Button/Button.jsx";
import style from "./style.css";
import PropTypes from 'prop-types';
import {Select} from "components/Select/Select.jsx";


/**
 * Form component for new or existing todos
 */
class TodoForm extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

        this.state = {title: props.title, note: props.note, date: props.date, priority: props.priority};
    }

    // handler invokes on every time when input is changed
    handleInputChange(event) {
        let {target} = event,
            value = target.value,
            name = target.name;

        // store input values in state
        this.setState({
            [name]: escape(value)
        });
    }


    // if all fields are valid - call handleSuccess from props
    handleSubmit(e) {
        let {title, note, date, priority} = this.state,
            {handleSuccess} = this.props;

        // if default browser validation pass - then invoke success callback
        if (title && priority && date) {
            e.preventDefault();
            handleSuccess({title, note, date, priority});
        }
    }

    render() {
        let today = new Date().toJSON().split('T')[0],
            {handleCancel} = this.props,
            {title, note, date, priority} = this.state;

        return (<form action="" className={style.form} onSubmit={this.handleSubmit}>
                <div className={style.fieldSet}>
                    <label htmlFor="title_field">Title:</label>
                    <input id="title_field" name="title" placeholder="Enter here task title" value={title}
                           onChange={this.handleInputChange} required/>
                </div>

                <div className={style.fieldSet}>
                    <label htmlFor="notes_field">Notes:</label>
                    <textarea rows="4" id="notes_field" name="note" placeholder="Optional notes" value={note}
                              onChange={this.handleInputChange}></textarea>
                </div>


                <div className={style.fieldSet}>
                    <label htmlFor="date_field">Deadline:</label>
                    <input id="date_field" name="date" type="date" min={today} value={date}
                           onChange={this.handleInputChange}
                           required/>
                </div>


                <div className={style.fieldSet}>
                    <label htmlFor="priority_field">Priority:</label>
                    <Select options={[
                        {value: "low", label: "Low priority"},
                        {value: "normal", label: "Normal priority"},
                        {value: "high", label: "High priority"}]}
                            selected={priority}
                            attrs={{id: "priority_field", name: "priority"}}
                            handleChange={this.handleInputChange}/>
                </div>

                <div className={style.formControls}>
                    <Button attrs={{title: "Add new task to list", type: "button"}} handleOnClick={handleCancel}
                            type="flat">Back</Button>

                    <Button attrs={{title: "Add new task to list", type: "submit"}} handleOnClick={this.handleSubmit}
                            type="success">Add To-Do</Button>
                </div>

            </form>
        );
    }
}

TodoForm.propTypes = {
    priority: PropTypes.oneOf(["low", "normal", "high"]),
    title: PropTypes.string,
    note: PropTypes.string,
    date: PropTypes.string,
}

TodoForm.defaultProps = {
    priority: "normal",
    title: "",
    note: "",
    date: ""
}

export {TodoForm}