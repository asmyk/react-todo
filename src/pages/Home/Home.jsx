import React, {Component} from "react";
import {Title} from "components/Title/Title.jsx";
import {Button} from "components/Button/Button.jsx";
import {Sort} from "components/Sort/Sort.jsx";
import {Search} from "components/Search/Search.jsx";
import style from "./style.css";
import routes from "utils/routes.js";
import {getState} from "services/LocalStorageService.js";
import {TodoList} from "containers/TodoList/TodoList.jsx";
import {saveTodo, removeTodo} from "services/LocalStorageService.js";
import {sortOptions} from "utils/sorters.js"
import PropTypes from 'prop-types';


/**
 * Home page component. Shows todo tasks list and manage them
 */
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {"todoList": [], searchText: "", sortStrategy: sortOptions[0].value};

    this.handleAddTodoClick = this.handleAddTodoClick.bind(this);
    this.handleUpdateItem = this.handleUpdateItem.bind(this);
    this.handleRemoveItem = this.handleRemoveItem.bind(this);
    this.handleSortChange = this.handleSortChange.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentDidMount() {
    // load tasks from storage
    this.updateTodoList();
  }

  // keep track on todo's state
  updateTodoList() {
    let state = getState(),
      list = [];

    if (typeof state === "object") {
      list = Object.values(state)
    }

    this.setState({"todoList": list})
  }

  // handle update task item
  handleUpdateItem(itemData) {
    saveTodo(itemData)
  }

  // remove task item from storage
  handleRemoveItem(id) {
    removeTodo(id);
    this.updateTodoList();
  }

  // User clicked on add new task button - redirect to add task form
  handleAddTodoClick() {
    this.props.history.push(routes.ADD_TODO);
  }

  // set sort strategy and trigger state update for sorting
  handleSortChange(sortStrategy) {
    this.setState({sortStrategy: sortStrategy});
  }

  // update filter text from search input
  handleSearchChange(text) {
    this.setState({searchText: text});
  }

  render() {
    let todoList = this.state.todoList,
      isAuthenticated = this.props.auth.isAuthenticated();

    return (
      <div>
        <div className={style.pageHeader}>
          <Title>My tasks: </Title>
          <Button handleOnClick={this.handleAddTodoClick} type="success">+</Button>
        </div>

        <div className={style.filtersContainer}>
          <Search value={this.state.searchValue} handleChange={this.handleSearchChange}/>
          <Sort sortStrategy={this.state.sortStrategy} handleSortBy={this.handleSortChange}/>
        </div>

        <TodoList handleRemoveItem={this.handleRemoveItem}
                  handleUpdateItem={this.handleUpdateItem}
                  sortStrategy={this.state.sortStrategy}
                  searchText={this.state.searchText}
                  isAuthenticated={isAuthenticated}
                  todoList={todoList}/>
      </div>
    )
  }
}

Home.propTypes = {
  auth: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};


export {
  Home
}