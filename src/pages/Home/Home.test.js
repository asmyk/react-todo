import React from 'react';
import ReactDOM from 'react-dom';
import {Home} from './Home';
import {Button} from "components/Button/Button.jsx";
import TestUtils from 'react-dom/test-utils';
import {createMemoryHistory} from 'history'

let requiredProps = {auth: {isAuthenticated: () => false}, history: createMemoryHistory("/ad")};

describe("home page", function () {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Home {...requiredProps}/>, div);
  });

  it("when click on add-button should redirect to add-todo", function () {
    Home.prototype.handleAddTodoClick = jest.genMockFunction();

    const home = TestUtils.renderIntoDocument(<Home {...requiredProps}/>);
    const addBtn = TestUtils.findRenderedDOMComponentWithTag(
      home, "button"
    );
    TestUtils.Simulate.click(addBtn);

    expect(Home.prototype.handleAddTodoClick).toBeCalled();
  });
});
