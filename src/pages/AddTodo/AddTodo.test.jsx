import React from 'react';
import ReactDOM from 'react-dom';
import {AddTodo} from './AddTodo';
import {createMemoryHistory} from 'history'

let requiredProps = {auth: {isAuthenticated: () => false}, history: createMemoryHistory("/ad")};

describe("add todo", function () {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<AddTodo {...requiredProps}/>, div);
  });

});
