import React, {Component} from "react";
import {Title} from "components/Title/Title.jsx";
import {TodoForm} from "containers/TodoForm/TodoForm";
import style from "./style.css";
import routes from "utils/routes";

import {saveTodo} from "services/LocalStorageService.js";
import PropTypes from 'prop-types';
import {Home} from "../Home/Home";

/**
 * Add to do page component. It shows form and send request to localstorage service to save
 */
class AddTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleFormSuccess = this.handleFormSuccess.bind(this);
    this.handleHomeRedirect = this.handleHomeRedirect.bind(this);
  }

  handleFormSuccess(formData) {
    saveTodo(formData);
    this.handleHomeRedirect();
  }

  handleHomeRedirect() {
    this.props.history.push(routes.HOME);
  }

  render() {

    return (<div>
      <Title>Add new task: </Title>

      <div className={style.formContainer}>
        <TodoForm handleSuccess={this.handleFormSuccess} handleCancel={this.handleHomeRedirect}/>
      </div>
    </div>);
  }
}

Home.propTypes = {
  history: PropTypes.object.isRequired
};


export {AddTodo}