import React from 'react';
import ReactDOM from 'react-dom';
import {NotSupportStorage} from './NotSupportStorage';
import TestUtils from 'react-dom/test-utils';
import {createMemoryHistory} from 'history'

describe("NotSupportStorage page", function () {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<NotSupportStorage/>, div);
  });

});
