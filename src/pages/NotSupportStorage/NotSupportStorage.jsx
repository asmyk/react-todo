import React from "react";

function NotSupportStorage() {
    return <h2>Unfortunettly your browser don't support Storage. I can't help you with your todos ;(</h2>
}

export {NotSupportStorage};