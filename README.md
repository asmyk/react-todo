Simple ToDo app written in React. For routing I used react-router and LocalStorage as database.
This app allows you create todos, manage them(with sort and live search) and remove. Also, login and social share is available.

For authentication I used oauth service, and set default callback to localhost:3000. This is set only for test purposes.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Please install last verision of node, and then install yarn by npm:

```
npm install -g yarn
```

### Installing

First you should install :
```
yarn
```

### Dev server

To run dev server:
```
yarn start
```
App should be available on http://localhost:3000


## Running the tests

I implemented only basic test cases. To run test:
```
yarn test
```

### Production version

For generate production build version:
```
yarn build
```
